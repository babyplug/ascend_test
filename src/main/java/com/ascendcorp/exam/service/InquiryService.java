package com.ascendcorp.exam.service;

import com.ascendcorp.exam.model.InquiryServiceResultDTO;
import com.ascendcorp.exam.model.TransferResponse;
import com.ascendcorp.exam.proxy.BankProxyGateway;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import javax.xml.ws.WebServiceException;
import java.util.Date;

public class InquiryService {

    @Autowired
    private BankProxyGateway bankProxyGateway;

    final static Logger log = Logger.getLogger(InquiryService.class);

    enum HandleException {
        NULL_POINTER,
        TIME_OUT,
        INTERNAL_APP_ERROR
    }

    public InquiryServiceResultDTO inquiry(String transactionId,
                                           Date tranDateTime,
                                           String channel,
                                           String locationCode,
                                           String bankCode,
                                           String bankNumber,
                                           double amount,
                                           String reference1,
                                           String reference2,
                                           String firstName,
                                           String lastName) {
        InquiryServiceResultDTO respDTO = null;
        try {
            log.info("validate request parameters.");

            if (transactionId == null) {
                log.info("Transaction id is required!");
                throw new NullPointerException("Transaction id is required!");
            }
            if (tranDateTime == null) {
                log.info("Transaction DateTime is required!");
                throw new NullPointerException("Transaction DateTime is required!");
            }
            if (channel == null) {
                log.info("Channel is required!");
                throw new NullPointerException("Channel is required!");
            }
            if (bankCode == null || bankCode.equalsIgnoreCase("")) {
                log.info("Bank Code is required!");
                throw new NullPointerException("Bank Code is required!");
            }
            if (bankNumber == null || bankNumber.equalsIgnoreCase("")) {
                log.info("Bank Number is required!");
                throw new NullPointerException("Bank Number is required!");
            }
            if (amount <= 0) {
                log.info("Amount must more than zero!");
                throw new NullPointerException("Amount must more than zero!");
            }

            log.info("call bank web service");
            TransferResponse response = bankProxyGateway.requestTransfer(transactionId, tranDateTime, channel,
                    bankCode, bankNumber, amount, reference1, reference2);

            log.info("check bank response code");
            if (response != null) // New
            {
                log.debug("found response code");
                respDTO = new InquiryServiceResultDTO();

                respDTO.setRef_no1(response.getReferenceCode1());
                respDTO.setRef_no2(response.getReferenceCode2());
                respDTO.setAmount(response.getBalance());
                respDTO.setTranID(response.getBankTransactionID());

                handleInquiryServiceResultDTOByResponseCode(response, respDTO);
            } else {
                // no response from bank
                throw new Exception("Unable to inquiry from service.");
            }
        } catch (NullPointerException ne) {
            if (respDTO == null) {
                respDTO = new InquiryServiceResultDTO();
                handleInquiryServiceResultDTOByExceptionType(HandleException.NULL_POINTER, respDTO);
            }
        } catch (WebServiceException r) {
            // handle error from bank web service
            String faultString = r.getMessage();
            if (respDTO == null) {
                respDTO = new InquiryServiceResultDTO();
                if (faultString != null && (faultString.contains("java.net.SocketTimeoutException")
                        || faultString.contains("Connection timed out"))) {
                    // bank timeout
                    handleInquiryServiceResultDTOByExceptionType(HandleException.TIME_OUT, respDTO);
                } else {
                    // bank general error
                    handleInquiryServiceResultDTOByExceptionType(HandleException.INTERNAL_APP_ERROR, respDTO);
                }
            }
        } catch (Exception e) {
            log.error("inquiry exception", e);
            if (respDTO == null || respDTO.getReasonCode() == null) {
                respDTO = new InquiryServiceResultDTO();
                handleInquiryServiceResultDTOByExceptionType(HandleException.INTERNAL_APP_ERROR, respDTO);
            }
        }
        return respDTO;
    }

    private void handleInquiryServiceResultDTOByResponseCode(TransferResponse response, InquiryServiceResultDTO respDTO) throws Exception {
        if (response != null && response.getResponseCode() != null) {
            switch (response.getResponseCode().toLowerCase()) {
                case "approved":
                    respDTO.setReasonDesc(response.getDescription());
                    respDTO.setAccountName(response.getDescription());
                    handleReasonCodeAndReasonDescByResponseCode(response, respDTO);
                    break;
                case "invalid_data":
                case "transaction_error":
                case "unknown":
                    handleReplyDescByDescription(response, respDTO);
                    break;
                default: // not_support
                    throw new Exception("Unsupport Error Reason Code");
            }
        }
    }

    private void handleReplyDescByDescription(TransferResponse response, InquiryServiceResultDTO respDTO) {
        String replyDesc = response.getDescription();
        if (replyDesc != null) {
            String[] respDesc = replyDesc.split(":");

            // if respDesc.length gte 3 is full format
            if (respDesc.length >= 3) {
                log.info("bank description is full format with length gte 3");
                log.info("reasonCode is respDesc[1]: " + respDesc[1] + "-> reasonDesc is respDesc[2]: " + respDesc[2]);
                // bank description full format
                respDTO.setReasonCode(respDesc[1]);
                respDTO.setReasonDesc(respDesc[2]);
            } else if (respDesc.length >= 2) {
                log.info("bank description is short format with length gte 2 for response code transaction_error and unknown");
                log.info("reasonCode is respDesc[0]: " + respDesc[0] + "-> reasonDesc is respDesc[1]: " + respDesc[1]);
                // bank description short format
                respDTO.setReasonCode(respDesc[0]);
                respDTO.setReasonDesc(respDesc[1]);
                if (respDTO.getReasonDesc() == null || respDTO.getReasonDesc().trim().length() == 0) {
                    respDTO.setReasonDesc("General Invalid Data");
                }
            } else {
                log.info("handle reason code and reason desc by default if bank description is short format with length lte 1 ");
                // bank description short format
                handleReasonCodeAndReasonDescByResponseCode(response, respDTO);
            }
        } else {
            log.info("handle bank no description for response code equal " + response.getResponseCode().toLowerCase());
            // bank no description
            handleReasonCodeAndReasonDescByResponseCode(response, respDTO);
        }
    }

    private void handleReasonCodeAndReasonDescByResponseCode(TransferResponse responseCode, InquiryServiceResultDTO respDTO) {
        switch (responseCode.getResponseCode().toLowerCase()) {
            case "approved":
                respDTO.setReasonCode("200");
                break;
            case "invalid_data":
                respDTO.setReasonCode("400");
                respDTO.setReasonDesc("General Invalid Data");
                break;
            case "transaction_error":
                respDTO.setReasonCode("500");
                respDTO.setReasonDesc("General Transaction Error");
                break;
            default: // unknown
                respDTO.setReasonCode("501");
                respDTO.setReasonDesc("General Invalid Data");
        }
    }

    private void handleInquiryServiceResultDTOByExceptionType(HandleException exception, InquiryServiceResultDTO respDTO) {
        switch (exception) {
            case NULL_POINTER:
                respDTO.setReasonCode("500");
                respDTO.setReasonDesc("General Invalid Data");
                break;
            case TIME_OUT:
                respDTO.setReasonCode("503");
                respDTO.setReasonDesc("Error timeout");
                break;
            default: // "INTERNAL_APP_ERROR"
                respDTO.setReasonCode("504");
                respDTO.setReasonDesc("Internal Application Error");
        }
    }

}
